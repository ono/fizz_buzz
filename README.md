# FizzBuzz implementation

## Set up

- Install Ruby 2.X and bundler gem
- Run `bundle install`

## Run tests

    % bundle exec rspec spec

## Try it on console

    % irb -I . -r "fizz_buzz.rb"
    % FizzBuzz.new.print 30

