describe FizzBuzz do
  subject(:fizz_buzz) { FizzBuzz.new }

  describe '#print' do
    it 'prints out the FizzBuzz computed values up to the given number' do
      expect{ fizz_buzz.print(3) }.to output(
        "1\n2\nFizz\n"
      ).to_stdout
    end
  end

  describe '#upto' do
    it 'returns array with FizzBuzz computed values up to the given number' do
      result = fizz_buzz.upto(15)

      expect(result).to eql([
        1, 2, 'Fizz', 4, 'Buzz', 'Fizz', 7, 8, 'Fizz', 'Buzz',
        11, 'Fizz', 13, 14, 'FizzBuzz'
      ])
    end
  end

  describe '#compute' do
    context 'with number that can be divisible by 3 and 5' do
      let(:numbers) { [15, 30, 45] }

      it 'returns "FizzBuzz"' do
        results = numbers.map { |num| fizz_buzz.compute(num) }
        expect(results).to eql(['FizzBuzz'] * 3)
      end
    end

    context 'with number that can be divisible by 3 but 5' do
      let(:numbers) { [3, 12, 48] }

      it 'returns "Fizz"' do
        results = numbers.map { |num| fizz_buzz.compute(num) }
        expect(results).to eql(['Fizz'] * 3)
      end
    end

    context 'with number that can be divisible by 5 but 3' do
      let(:numbers) { [10, 20, 85] }

      it 'returns "Buzz"' do
        results = numbers.map { |num| fizz_buzz.compute(num) }
        expect(results).to eql(['Buzz'] * 3)
      end
    end

    context 'with string' do
      let(:number) { "3" }

      it 'converts the input to integer and then computes' do
        expect(fizz_buzz.compute(number)).to eql('Fizz')
      end
    end
  end
end
