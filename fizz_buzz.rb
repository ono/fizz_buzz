# http://www.codingdojo.org/cgi-bin/index.pl?KataFizzBuzz
#
class FizzBuzz
  def print(number)
    puts upto(number).join("\n")
  end

  def upto(number)
    1.upto(number.to_i).map do |i|
      compute i
    end
  end

  def compute(number)
    number = number.to_i
    if number % 15 == 0
      "FizzBuzz"
    elsif number % 5 == 0
      "Buzz"
    elsif number % 3 == 0
      "Fizz"
    else
      number
    end
  end
end
